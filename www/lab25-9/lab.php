

<!DOCTYPE html>
<html>

<body>
  <script>
    // 'use strict';
    // let fruit = prompt("Which fruit to buy?", "apple");
    
    // let bag = {
    //   [fruit]: 5, // the name of the property is taken from the variable fruit
    // };
    
    // alert( bag.apple ); // 5 if fruit="apple"


// 'use strict'
//     function makeUser(name, age) {
//   return {
//     name: name,
//     age: age
//     // ...other properties
//   };
// }

// let user = makeUser("John", 30);
// user = makeUser("JK",21);
// alert(user.age); // John

'use strict'
// // explicit conversion
// let num = Number(obj);

// // maths (except binary plus)
// let n = +obj; // unary plus
// let delta = date1 - date2;

// // less/greater comparison
// let greater = user1 > user2;

let str = 'Hi';

str = 'h' + str[1];  // replace the string

alert( str ); // hi
  </script>
</body>

</html>