<!DOCTYPE html>
<html lang="en">
<head>
	<title>Example of PHP GET method</title>
</head>
<body>


<?php
/*
$txt = "Hello World";
$number = 10;
echo $txt;
echo $number; 


echo "<h4>This is a simple heading.</h4>";
echo "<h4 style='color: red;'>This is heading with style/</h4>";


$txt = "Hello World";
$num = 123456789;
$colors = array("red","Green","Blue");

echo $txt;
echo "<br>";
echo $num;
echo "<br>";
echo $colors[0];


$a =123;
var_dump($a);
echo "<br>";

$b = -123;
var_dump($b);
echo "<br>";

$c =0x1A;
var_dump($c);
echo "<br>";

$d =0123;
var_dump($d);
//echo "<br>";


$colors = array("red","Green","Blue");
var_dump($colors);
echo "<br>";
*/

/**
 * 
 
class greeting
{
	public $str = "Hello World!";
	function _show_greeting()
	{
		return $this->str;
	}
}
$gg =date("D");
$message = new greeting;
var_dump($gg);
echo "<br>";
echo $message->_show_greeting();
*/

if(isset($_POST["name"])){
	echo "<p>Hi, " . $_POST["name"] . "</p>";
}
?>
<form method="POST" action="<?php echo $_SERVER["PHP_SELF"];?>">
<label for="inputName">Name:</label> 
<input type="text" name="name" id="inputName">
<input type="submit" name="Submit">
	
</form>
</body>
</html>